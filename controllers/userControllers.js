const User = require("../models/User.js");

// CREATE USERS
module.exports.createUserController = (req, res) => {

	console.log(req.body);

	User.findOne({username: req.body.username}).then(result => {

		console.log(result)

		if(result !== null && result.username === req.body.username){
			return res.send("Duplicate user found")

		} else {

			let newUser = new User({
				username: req.body.username,
				password: req.body.password
			})

			newUser.save()
			.then(result => res.send(result))
			.catch(result => res.send(error))
		}
	})
	.catch(error => res.send(error))
};

// RETRIEVE ALL USERS
module.exports.getAllUsersController = (req, res) => {

	User.find({})
	.then(result => res.send(result))
	.catch(result => res.send(error));
}

// UPDATING SINGLE USERNAME
module.exports.updateUserUsernameController = (req, res) => {

	console.log(req.params.id);
	console.log(req.body);

	let updates = {
		username: req.body.username
	};

	User.findByIdAndUpdate(req.params.id, updates, {new: true})
	.then(updatedUser => res.send(updatedUser))
	.catch(error => res.send(error));
};

// GET SINGLE USER
module.exports.getSingleUserController = (req, res) => {

	console.log(req.params)

	User.findById(req.params.id)
	.then(result => res.send(result))
	.catch(error => res.send(error))
}