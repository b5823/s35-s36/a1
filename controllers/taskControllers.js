// Import the Task model in our controller. So that our controllers or controller functions may have access to our Task model.
const Task = require("../models/Task.js");

// Create task

// createTaskController --> name of controller
// endgoal ---> create tasks without duplicate names
module.exports.createTaskController = (req, res) => {

	// checking captured data from request body
	console.log(req.body);

	// MongoDB: db.tasks.findOne({name: "nameOfTask"}) --> Robo3T
	// Converted into Mongoose method
	// then(anonymous function)
	Task.findOne({name: req.body.name}).then(result => {

		// checking the captured data from .then()
		console.log(result)
		// expected output:
			/*{
				_ObjectId: <randomAlphaNumericKeys>,
				name: "nameOfTask",
				status: "pending"
				}
			*/

		if(result !== null && result.name === req.body.name){
			return res.send("Duplicate task found")

		} else {

			let newTask = new Task({
				name: req.body.name,
				status: req.body.status
			})

			newTask.save()
			.then(result => res.send(result))
			.catch(error => res.send(error))
		}
	})
	.catch(error => res.send(error))
};

// Retrieve ALL tasks

module.exports.getAllTasksController = (req, res) => {

	// similar: db.tasks.find({})
	Task.find({})
	.then(result => res.send(result))
	.catch(error => res.send(error));
};

// RETRIEVAL SINGLE TASK

module.exports.getSingleTaskController = (req, res) => {

	console.log(req.params)

	// db.tasks.findone({_id: "id"})
	Task.findById(req.params.id)
	.then(result => res.send(result))
	.catch(error => res.send(error));
};

// Updating task status

module.exports.updateTaskStatusController = (req, res) => {

	console.log(req.params.id);
	console.log(req.body);

	let updates = {
		status: req.body.status
	};

	// findbyIdAndUpdate()
		// 3 arguents
			// a. where will you get the id? req.params.id
			// b. what is the update? updates variable
			// c. optional: {new: true} - return the update version of the document we are updating
	Task.findByIdAndUpdate(req.params.id, updates, {new: true})
	.then(updatedTask => res.send(updatedTask))
	.catch(error => res.send(error));
};
