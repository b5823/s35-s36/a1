const express = require("express");
const router = express.Router();

const userControllers = require("../controllers/userControllers.js")

console.log(userControllers);

// CREATE USERS
router.post("/", userControllers.createUserController)

module.exports = router;

// RETRIEVE ALL USERS
router.get("/", userControllers.getAllUsersController);

module.exports = router;

// UPDATE SINGLE USERNAME
router.put("/updateUser/:id", userControllers.updateUserUsernameController);

module.exports = router;

// GET SINGLE USER
router.get("/getSingleUser/:id", userControllers.getSingleUserController);

module.exports = router;