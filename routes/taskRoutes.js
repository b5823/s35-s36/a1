
const express = require("express");
const router = express.Router();
// Router method - allows access to HTTP methods

const taskControllers = require("../controllers/taskControllers.js")

console.log(taskControllers);

// create task route
router.post("/", taskControllers.createTaskController)

module.exports = router;

// get all tasks route
router.get("/", taskControllers.getAllTasksController);

module.exports = router;

// get single task
router.get("/getSingleTask/:id", taskControllers.getSingleTaskController);

module.exports = router;

// update task status
router.put("/updateTask/:id", taskControllers.updateTaskStatusController);

module.exports = router;
