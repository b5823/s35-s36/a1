// required modules
const express = require("express");

/*
	it allows us to connect in our mongoDB
*/
const mongoose = require("mongoose");

// port
const port = 4000;

// server
const app = express()

// mongoose connection

mongoose.connect("mongodb+srv://admin:admin@wdc028-course-booking.r8ofa.mongodb.net/tasks182?retryWrites=true&w=majority",
	{	
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);
// this will create a notification if the db connection is successful or not
let db = mongoose.connection

db.on("error", console.error.bind(console, "DB Connection Error"));

db.once("open", () => console.log("Successfully connected to MongoDB"));

// middlewares
app.use(express.json());

// reading of data forms
// usually string or array are being accepted, with this middleware, it will enable us to accept other data types
app.use(express.urlencoded({extended: true}))

// Routes
// grouping routes for tasks
const taskRoutes = require("./routes/taskRoutes");
app.use("/tasks", taskRoutes);

// GROUP ROUTING FOR ACTIVITY 1: CREATE/POST
const userRoutes = require("./routes/userRoutes");
app.use("/users", userRoutes);

// port listener
app.listen(port, () => console.log(`Server is running at port ${port}`))


/*
>> Create a new schema for User. It should have the following fields:
       --username,
       --password
   The data types for both fields is String.

>> Create a new model out of your schema and save it in a variable called User

>> Create a new POST method route to create 3 new user documents:
       -endpoint: "/users"
       -This route should be able to create a new user document.
       -Then, send the result in the client.
       -Catch an error while saving, send the error in the client.
       -STRETCH GOAL: no duplicate usernames

>> Create a new GET method route to retrieve all user documents:
       -endpoint: "/users"
       -Then, send the result in the client.
       -Catch an error, send the error in the client.

>> Make sure to group your routes for users in index.js
*/

// ---------------------> ACTIVITY 2 <--------------------->
/*
Activity 2:

>> Create 2 new routes and controllers in our userRoutes and userControllers.

>> Create a route and controller to update a single user's username field to our input from a request body.
 -endpoint: '/:id'
 -Create a new controller which is able to get the id from the url through the use of req.params:
     -Create an updates object and add the new value from our req.body as the new value to the username field.

     -Add a findByIdAndUpdate method from the User model and pass the id from req.params as its first argument.

     -Add the updates object as its second argument.

     -Add {new:true} as its third argument so the result would return the updated document instead of the old one.

     -Then pass the result to the client.

     -Catch the error and pass the error to the client.

 
>> Create a route and controller to get a single user.
     -endpoint: '/getSingleUser/:id'
     
     -Create a new controller which is able to get the id of the user from the url through the use of req.params.
         -In the controller, add a findById method from the User model and pass the id from req.params as its argument.
         -Then, send the result to the client.
         -Catch the error and send the error to the client.
         */